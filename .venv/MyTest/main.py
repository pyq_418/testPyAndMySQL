#测试
import MyTest.InteractiveModules
from datetime import datetime

def userMenu(user=None):
    if user == None:
        print("ERROR")
        return
    print(f"用户名：{user.getUserName()}")
    print(f"用户ID：{user.getAdminAccount()}")
    print("用户菜单：")
    print("1. 修改密码")
    print("2. 修改个人名称")
    print("3. 看查货架")
    print("4. 看查供应商")
    print("5. 看查订单")
    print("6. 添加新货物")
    print("7. 添加新订单")
    print("8. 修改产品库存")
    print("9. 修改产品售价")
    print("10. 修改产品名称")
    if user.getUserType() == 0:
        print("11. 修改供应商名称")
        print("12. 修改供应商地址")
        print("13. 添加新供应商")
        print("14. 删除供应商")
        print("15. 删除订单中指定供应商产品")
        print("16. 删除货架中指定产品")
        print("17. 修改供应价")
        print("18. 修改供应日期")
    print("END: 退出")

    while True:
        choice = input("请输入选择：")
        if choice == "1":
            newPassword = input("请输入新密码：")
            if user.setUserPassword(newPassword=newPassword):
                print("密码修改成功")
        elif choice == "2":
            newName = input("请输入新名称：")
            if user.setUserName(newName=newName):
                print("用户名修改成功")
        elif choice == "3":
            user.getAdminMessage().showProductsOnSale()
        elif choice == "4":
            user.getAdminMessage().showVendorsInfo()
        elif choice == "5":
            user.getAdminMessage().showProducts()
        elif choice == "6":
            newName = input("请输入新名称：")
            newPrice = input("请输入新价格：")
            newInventory = input("请输入新库存：")
            newPrice = float(newPrice)
            user.getAdminMessage().addNewProduct(productName=newName, productPrice=newPrice, productInventory=newInventory)
        elif choice == "7":
            productName = input("请输入产品名称：")
            vendorName = input("请输入供应商名称：")
            supplyPrice = input("请输入供应价：")
            supplyInventory = input("请输入供应库存：")
            supplyInventory = int(supplyInventory)
            supplyDate = input("请输入供应日期(如2023-04-01)：")
            expirationDays = input("请输入过期日期(如2023-04-01)：")
            supplyPrice = float(supplyPrice)
            sDate = datetime.strptime(supplyDate, "%Y-%m-%d")
            eDate = datetime.strptime(expirationDays, "%Y-%m-%d")
            user.getAdminMessage().addNewSupplierProduct(productName=productName, vendorName=vendorName, supplyPrice=supplyPrice, supplyInventory=supplyInventory, supplyDate=sDate, expirationDays=eDate)
        elif choice == "8":
            productName = input("请输入需要修改的产品名称：")
            newInventory = input("请输入新库存(含正负)：")
            newInventory = int(newInventory)
            user.getAdminMessage().changeProductInventory(productName=productName, number=newInventory)
        elif choice == "9":
            productName = input("请输入需要修改的产品名称：")
            newPrice = input("请输入新价格：")
            newPrice = float(newPrice)
            user.getAdminMessage().changeProductPrice(productName=productName, newPrice=newPrice)
        elif choice == "10":
            productName = input("请输入需要修改的产品名称：")
            newName = input("请输入新名称：")
            user.getAdminMessage().changeProductName(productName=productName, newName=newName)
        elif choice == "11" and user.getUserType() == 0:
            vendorName = input("请输入需要修改的供应商名称：")
            newName = input("请输入新名称：")
            user.getAdminMessage().changeVendorName(vendorName=vendorName, newName=newName)
        elif choice == "12" and user.getUserType() == 0:
            vendorName = input("请输入需要修改的供应商名称：")
            newAddress = input("请输入新地址：")
            user.getAdminMessage().changeVendorAddress(vendorName=vendorName, newAddress=newAddress)
        elif choice == "13" and user.getUserType() == 0:
            newName = input("请输入新供应商名称：")
            newAddress = input("请输入新地址：")
            user.getAdminMessage().addNewVendor(vendorName=newName, vendorAddress=newAddress)
        elif choice == "14" and user.getUserType() == 0:
            vendorName = input("请输入需要删除的供应商名称：")
            tmp = input("请输入确认删除[Y/N]：")
            if tmp == "Y":
                user.getAdminMessage().deleteVendor(vendorName=vendorName)
        elif choice == "15" and user.getUserType() == 0:
            vendorName = input("请输入需要删除的供应商名称：")
            productName = input("请输入需要删除的产品名称：")
            tmp = input("请输入确认删除[Y/N]：")
            if tmp == "Y":
                user.getAdminMessage().deleteSupplierProduct(vendorName=vendorName, productName=productName)
        elif choice == "16" and user.getUserType() == 0:
            productName = input("请输入需要删除的产品名称：")
            tmp = input("请输入确认删除[Y/N]：")
            if tmp == "Y":
                user.getAdminMessage().deleteProduct(productName=productName)
        elif choice == "17" and user.getUserType() == 0:
            vendorName = input("请输入需要修改的产品的供应商名称：")
            productName = input("请输入需要修改的产品名称：")
            newPrice = input("请输入新价格：")
            newPrice = float(newPrice)
            user.getAdminMessage().changeProductSupplyPrice(productName=productName, vendorName=vendorName, supplyPrice=newName)
        elif choice == "18" and user.getUserType() == 0:
            vendorName = input("请输入需要修改的产品的供应商名称：")
            productName = input("请输入需要修改的产品名称：")
            newDate = input("请输入新日期(如2023-04-01)：")
            sDate = datetime.strptime(newDate, "%Y-%m-%d")
            user.getAdminMessage().changeSupplyDate(productName=productName, vendorName=vendorName, supplyDate=sDate)
        elif choice.upper() == "END":
            break

    return


condition = True

while condition:
    accout = input("输入账号(输入exit退出)：")
    if accout == "exit":
        break
    pssword = input("输入密码：")

    passport = MyTest.InteractiveModules.AuthenticationService(accout, pssword)

    if passport.verifyIdentity() == True:

        user = MyTest.InteractiveModules.UserSession(AdminAccount=accout)
        userMenu(user=user)

        condition = False
        break

print("程序结束")
