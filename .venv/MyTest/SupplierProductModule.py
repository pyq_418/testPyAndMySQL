import pymysql
import MyTest.VendorsModule as VendorsModule
import MyTest.ProductModule as ProductModule
from MyTest.db_connection import get_db_connection

class SupplierProductDAO():

    @staticmethod
    def getAllSupplierProducts():
        """
        从数据库中获取所有供应商供应的商品信息。

        Args:
            无。

        Returns:
            List[tuple]: 包含所有供应商供应的商品信息的元组列表，每个元组代表一个产品记录。
            如果查询过程中发生异常，则返回None。

        Raises:
            无。
        """
        try:
           connection = get_db_connection()
           with connection.cursor() as cursor:
               cursor.execute('SELECT * FROM supplierproduct')
               return cursor.fetchall()
        except Exception as e:
            print(f"Error: {e}")
            return None

    @staticmethod
    def getAllProductsBySupplier(supplierID=None, supplierName=None):
        """
        根据供应商ID或供应商名称获取供应商供应的商品信息。
        Args:
            supplierID (int, optional): 供应商ID。
            supplierName (str, optional): 供应商名称。
        Returns:
            List[tuple]: 包含所有供应商供应的商品信息的元组列表，每个元组代表一个产品记录。
            如果查询过程中发生异常，则返回None。
        Raises:
            无。
        """
        if supplierID is None and supplierName is None:
            return None
        try:
            connection = get_db_connection()
            with connection.cursor() as cursor:
                if supplierID is not None:
                    id = supplierID # 根据供应商ID查询
                else:
                    id = VendorsModule.VendorsRepository.getVendorId(vendorName=supplierName) # 获取供应商ID
                if id is None:
                    return None # 供应商ID不存在
                cursor.execute('SELECT * FROM supplierproduct WHERE supplierID=%s', id)
                return cursor.fetchall()
        except Exception as e:
            print(f"Error: {e}")
            return None

    @staticmethod
    def addNewProduct(newProduct=None):
        """
        向数据库中添加新供应商供应的商品信息。
        Args:
            newProduct (dict, optional): 包含新供应商供应的商品信息的字典，格式如下：
                {
                    'productName': '产品名称',
                    'vendorName': '供应商名称',
                    'supplyPrice': '供应价格',
                    'supplyInventory': '供应库存',
                    'supplyDate': '供应日期',
                    'expirationDays': '过期日期'
                }
        Returns:
            bool: True表示成功，False表示失败。
        Raises:
            无。
        """
        if newProduct is None:
            return False
        productName = newProduct['productName']
        supplierName = newProduct['vendorName']
        supplyPrice = newProduct['supplyPrice']
        supplyInventory = newProduct['supplyInventory']
        supplyDate = newProduct['supplyDate']
        expirationDays = newProduct['expirationDays']
        productId = ProductModule.ProductRepository.getProductId(productName=productName)
        supplierId = VendorsModule.VendorsRepository.getVendorId(vendorName=supplierName)
        if productId is None and supplierId is None:
            return False # 商品或供应商不存在
        try:
            connection = get_db_connection()
            with connection.cursor() as cursor:
                cursor.execute('INSERT INTO '
                               'supplierproduct(productId, vendorId, SupplyPrice, SupplyInventory, SupplyDate, ExpirationDays) '
                               'VALUES (%s, %s, %s, %s, %s, %s)', (productId, supplierId, supplyPrice, supplyInventory, supplyDate, expirationDays))
                connection.commit() # 提交事务
                return True
        except Exception as e:
            print(f"Error: {e}")
            return False

    @staticmethod
    def deleteProduct(productName=None, vendorName=None):
        """
        根据商品名称和供应商名称删除数据库中对应记录。
        Args:
            productName (str, optional): 商品名称。
            vendorName (str, optional): 供应商名称。
        Returns:
            bool: True表示成功，False表示失败。
        Raises:
            无。
        """
        if productName is None and vendorName is None:
            return None # 商品或供应商不存在
        productId = ProductModule.ProductRepository.getProductId(productName=productName)
        venderId = VendorsModule.VendorsRepository.getVendorId(vendorName=vendorName)
        if productId is None and venderId is None:
            return False # 商品或供应商不存在
        try:
            connection = get_db_connection()
            with connection.cursor() as cursor:
                cursor.execute('DELETE FROM supplierproduct WHERE productId=%s AND vendorId=%s', (productId, venderId))
                connection.commit() # 提交事务
                return True
        except Exception as e:
            print(f"Error: {e}")
            return False

    @staticmethod
    def getProductInformationbySupplier(productName=None, vendorName=None):
        """
        根据商品名称和供应商名称查询数据库中对应记录。
        Args:
            productName (str, optional): 商品名称。
            vendorName (str, optional): 供应商名称。
        Returns:
            tuple | None: 包含该供应商供应的商品信息的元组，如果查询过程中发生异常，则返回None。
        Raises:
            无。
        """
        if productName is None and vendorName is None:
            return None # 商品或供应商不存在
        productId = ProductModule.ProductRepository.getProductId(productName=productName)
        venderId = VendorsModule.VendorsRepository.getVendorId(vendorName=vendorName)
        if productId is None and venderId is None:
            return False # 商品或供应商不存在
        try:
            connection = get_db_connection()
            with connection.cursor() as cursor:
                cursor.execute('SELECT * FROM supplierproduct WHERE productId=%s AND vendorId=%s',
                               (productId, venderId))
                return cursor.fetchall()
        except Exception as e:
            print(f"Error: {e}")
            return None

    @staticmethod
    def updateProductPrice(productName=None, vendorName=None, newPrice=None):
        """
        根据商品名称和供应商名称更新数据库中对应记录的供应价格。
        Args:
            productName (str, optional): 商品名称。
            vendorName (str, optional): 供应商名称。
            newPrice (float, optional): 新的供应价格。
        Returns:
            bool: True表示成功，False表示失败。
        Raises:
            无。
        """
        if productName is None and vendorName is None:
            return False # 商品或供应商不存在
        if newPrice is None:
            return False # 价格不能为空
        productId = ProductModule.ProductRepository.getProductId(productName=productName)
        venderId = VendorsModule.VendorsRepository.getVendorId(vendorName=vendorName)
        if productId is None and venderId is None:
            return False # 商品或供应商不存在
        try:
            connection = get_db_connection()
            with connection.cursor() as cursor:
                cursor.execute('UPDATE supplierproduct SET '
                               'SupplyPrice=%s WHERE productId=%s AND vendorId=%s',
                               (newPrice, productId, venderId))
                connection.commit() # 提交事务
                return True
        except Exception as e:
            print(f"Error: {e}")
            return False

    @staticmethod
    def updateProductInventory(productName=None, vendorName=None, newInventory=None):
        """
        根据商品名称和供应商名称更新数据库中对应记录的供应数量。
        Args:
            productName (str, optional): 商品名称。
            vendorName (str, optional): 供应商名称。
            newInventory (int, optional): 新的供应数量。
        Returns:
            bool: True表示成功，False表示失败。
        Raises:
            无。
        """
        if productName is None and vendorName is None:
            return False # 商品或供应商不存在
        if newInventory is None:
            return False # 数量不能为空
        productId = ProductModule.ProductRepository.getProductId(productName=productName)
        venderId = VendorsModule.VendorsRepository.getVendorId(vendorName=vendorName)
        if productId is None and venderId is None:
            return False # 商品或供应商不存在
        try:
            connection = get_db_connection()
            with connection.cursor() as cursor:
                cursor.execute('UPDATE supplierproduct SET '
                               'SupplyInventory=%s WHERE productId=%s AND vendorId=%s',
                               (newInventory, productId, venderId))
                connection.commit() # 提交事务
                return True
        except Exception as e:
            print(f"Error: {e}")
            return False

    @staticmethod
    def updateProductSupplyDate(productName=None, vendorName=None, newSupplyDate=None):
        """
        根据商品名称和供应商名称更新数据库中对应记录的供应日期。
        Args:
            productName (str, optional): 商品名称。
            vendorName (str, optional): 供应商名称。
            newSupplyDate (datetime.date, optional): 新的供应日期。
        Returns:
            bool: True表示成功，False表示失败。
        Raises:
            无。
        """
        if productName is None and vendorName is None:
            return False # 商品或供应商不存在
        if newSupplyDate is None:
            return False # 日期不能为空
        productId = ProductModule.ProductRepository.getProductId(productName=productName)
        venderId = VendorsModule.VendorsRepository.getVendorId(vendorName=vendorName)
        if productId is None and venderId is None:
            return False # 商品或供应商不存在
        try:
            connection = get_db_connection()
            with connection.cursor() as cursor:
                cursor.execute('UPDATE supplierproduct SET SupplyDate=%s WHERE productId=%s AND vendorId=%s', (newSupplyDate, productId, venderId))
                connection.commit() # 提交事务
                return True
        except Exception as e:
            print(f"Error: {e}")
            return False

    @staticmethod
    def updateProductExpirationDays(productName=None, vendorName=None, newExpirationDays=None):
        """
        根据商品名称和供应商名称更新数据库中对应记录的过期日期。
        Args:
            productName (str, optional): 商品名称。
            vendorName (str, optional): 供应商名称。
            newExpirationDays (int, optional): 新的到期天数。
        Returns:
            bool: True表示成功，False表示失败。
        Raises:
            无。
        """
        if productName is None and vendorName is None:
            return False # 商品或供应商不存在
        if newExpirationDays is None:
            return False # 日期不能为空
        productId = ProductModule.ProductRepository.getProductId(productName=productName)
        venderId = VendorsModule.VendorsRepository.getVendorId(vendorName=vendorName)
        if productId is None and venderId is None:
            return False # 商品或供应商不存在
        try:
            connection = get_db_connection()
            with connection.cursor() as cursor:
                cursor.execute('UPDATE supplierproduct SET ExpirationDays=%s WHERE productId=%s AND vendorId=%s', (newExpirationDays, productId, venderId))
                connection.commit() # 提交事务
                return True
        except Exception as e:
            print(f"Error: {e}")
            return False

class SupplierProductService():
    def __init__(self):
        pass

    def getSupplierProducts(self):
        """
        获取所有供应商商品信息。
        Args:
            无。
        Returns:
            list: 返回所有供应商商品信息字典。
        Raises:
            无。
        """
        return SupplierProductDAO.getAllSupplierProducts()

    def getProductByVendorName(self, vendorId=None, vendorName=None):
        """
        根据供应商名称获取该供应商的所有商品信息。
        Args:
            vendorId (int, optional): 供应商ID。
            vendorName (str, optional): 供应商名称。
        Returns:
            list: 返回所有供应商商品信息元组列表。
        Raises:
            无。
        """
        return SupplierProductDAO.getAllProductsBySupplier(supplierName=vendorName)

    def addNewProduct(self, newProduct):
        """
        添加新商品。
        Args:
            newProduct (dict, optional): 包含新供应商供应的商品信息的字典，格式如下：
                {
                    'productName': '产品名称',
                    'vendorName': '供应商名称',
                    'supplyPrice': '供应价格',
                    'supplyInventory': '供应库存',
                    'supplyDate': '供应日期',
                    'expirationDays': '过期日期'
                }
        Returns:
            bool: True表示成功，False表示失败。
        Raises:
            无。
        """
        return SupplierProductDAO.addNewProduct(newProduct=newProduct)

    def deleteProduct(self, vendorName=None, productName=None):
        """
        根据供应商名称和商品名称删除该供应商的供应记录。
        Args:
            vendorName (str, optional): 供应商名称。
            productName (str, optional): 商品名称。
        Returns:
            bool: True表示成功，False表示失败。
        Raises:
            无。
        """
        return SupplierProductDAO.deleteProduct(vendorName=vendorName, productName=productName)

    def getProductInfo(self, vendorName=None, productName=None):
        """
        根据供应商名称和商品名称获取该供应商的供应记录。
        Args:
            vendorName (str, optional): 供应商名称。
            productName (str, optional): 商品名称。
        Returns:
            list: 返回供应商商品信息元组。
        Raises:
            无。
        """
        return SupplierProductDAO.getProductInformationbySupplier(vendorName=vendorName, productName=productName)

    def updateProductPrice(self, productName=None, vendorName=None, newPrice=None):
        """
        根据供应商名称和商品名称更新该供应商的供应记录价格。
        Args:
            productName (str, optional): 商品名称。
            vendorName (str, optional): 供应商名称。
            newPrice (int, optional): 新价格。
        Returns:
            bool: True表示成功，False表示失败。
        Raises:
            无。
        """
        return SupplierProductDAO.updateProductPrice(productName=productName, vendorName=vendorName, newPrice=newPrice)

    def updateProductInventory(self, productName=None, vendorName=None, newInventory=None):
        """
        根据供应商名称和商品名称更新该供应商的供应记录库存。
        Args:
            productName (str, optional): 商品名称。
            vendorName (str, optional): 供应商名称。
            newInventory (int, optional): 新库存。
        Returns:
            bool: True表示成功，False表示失败。
        Raises:
            无。
        """
        return SupplierProductDAO.updateProductInventory(productName=productName, vendorName=vendorName, newInventory=newInventory)

    def updateProductSupplyDate(self, productName=None, vendorName=None, newSupplyDate=None):
        """
        根据供应商名称和商品名称更新该供应商的供应记录日期。
        Args:
            productName (str, optional): 商品名称。
            vendorName (str, optional): 供应商名称。
            newSupplyDate (int, optional): 新日期。
        Returns:
            bool: True表示成功，False表示失败。
        Raises:
            无。
        """
        return SupplierProductDAO.updateProductSupplyDate(productName=productName, vendorName=vendorName, newSupplyDate=newSupplyDate)

    def updateProductExpirationDays(self, productName=None, vendorName=None,newExpirationDays=None):
        """
        根据供应商名称和商品名称更新该供应商的供应记录过期天数。
        Args:
            productName (str, optional): 商品名称。
            vendorName (str, optional): 供应商名称。
            newExpirationDays (int, optional): 新过期天数。
        Returns:
            bool: True表示成功，False表示失败。
        Raises:
            无。
        """
        return SupplierProductDAO.updateProductExpirationDays(productName=productName, vendorName=vendorName, newExpirationDays=newExpirationDays)
