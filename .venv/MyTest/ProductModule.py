import pymysql
from MyTest.db_connection import get_db_connection

class ProductRepository():

    @staticmethod
    def getAllProducts():
        """
        从数据库中获取所有产品信息。

        Args:
            无。

        Returns:
            List[tuple]: 包含所有产品信息的元组列表，每个元组代表一个产品记录。
            如果查询过程中发生异常，则返回None。

        Raises:
            无。
        """

        try:
            connection = get_db_connection()
            with connection.cursor() as cursor:
                cursor.execute('SELECT * FROM Product')
                return cursor.fetchall()
        except Exception as e:
            print(f"Error: {e}")
            return None

    @staticmethod
    def getAllProductsName():
        """
        从数据库中获取所有产品名称。

        Args:
            无。

        Returns:
            List[str]: 包含所有产品名称的列表，如果查询过程中发生异常，则返回None。
        Raises:
            无。
        """
        try:
            connection = get_db_connection()
            with connection.cursor() as cursor:
                cursor.execute('SELECT ProductName FROM Product')
                return [row['ProductName'] for row in cursor.fetchall()]
        except Exception as e:
            print(f"Error: {e}")
            return None

    @staticmethod
    def getAllProductsPrice():
        """
        从数据库中获取所有产品价格。
        Args:
            无。
        Returns:
            List[float]: 包含所有产品价格的列表，如果查询过程中发生异常，则返回None。
        Raises:
            无。
        """
        try:
            connection = get_db_connection()
            with connection.cursor() as cursor:
                cursor.execute('SELECT ProductPrice FROM Product')
                return [row['ProductPrice'] for row in cursor.fetchall()]
        except Exception as e:
            print(f"Error: {e}")
            return None

    @staticmethod
    def getAllProductsInventory():
        """
        从数据库中获取所有产品库存。
        Args:
            无。
        Returns:
            List[int]: 包含所有产品库存的列表，如果查询过程中发生异常，则返回None。
        Raises:
            无。
        """
        try:
            connection = get_db_connection()
            with connection.cursor() as cursor:
                cursor.execute('SELECT ProductInventory FROM Product')
                return [row['ProductInventory'] for row in cursor.fetchall()]
        except Exception as e:
            print(f"Error: {e}")
            return None

    @staticmethod
    def getProductInformation(productName):
        """
        根据产品名称获取产品信息。
        Args:
            productName (str): 产品名称。
        Returns:
            dict | None: 包含产品信息的字典，如果查询过程中发生异常，则返回None。
        Raises:
            无。
        """
        try:
            connection = get_db_connection()
            with connection.cursor() as cursor:
                cursor.execute('SELECT * FROM Product WHERE ProductName = %s', productName)
                return cursor.fetchone()
        except Exception as e:
            print(f"Error: {e}")
            return None

    @staticmethod
    def getProductName(productId=None, productName=None):
        """
        根据产品ID或名称获取产品名称。
        Args:
            productId (int): 产品ID，如果为None则忽略此参数。
            productName (str): 产品名称，如果为None则忽略此参数。
        Returns:
            str | None: 产品名称，如果查询过程中发生异常，则返回None。
        Raises:
            无。
        """
        if productId is None and productName is None:
            return None
        try:
            connection = get_db_connection()
            with connection.cursor() as cursor:
                if productId is not None:
                    cursor.execute('SELECT ProductName FROM Product WHERE ProductId = %s', productId)
                else:
                    cursor.execute('SELECT ProductName FROM Product WHERE ProductName = %s', productName)
                return cursor.fetchone()['ProductName']
        except Exception as e:
            print(f"Error: {e}")
            return None

    @staticmethod
    def getProductId(productId=None, productName=None):
        """
        根据产品ID或名称获取产品ID。
        Args:
            productId (int): 产品ID，如果为None则忽略此参数。
            productName (str): 产品名称，如果为None则忽略此参数。
        Returns:
            int | None: 产品ID，如果查询过程中发生异常，则返回None。
        Raises:
            无。
        """
        if productId is None and productName is None:
            return None
        try:
            connection = get_db_connection()
            with connection.cursor() as cursor:
                if productId is not None:
                    cursor.execute('SELECT ProductId FROM Product WHERE ProductId = %s', productId)
                else:
                    cursor.execute('SELECT ProductId FROM Product WHERE ProductName = %s', productName)
                return cursor.fetchone()['ProductId']
        except Exception as e:
            print(f"Error: {e}")
            return None

    @staticmethod
    def getProductPrice(productId=None, productName=None):
        """
        根据产品ID或名称获取产品价格。
        Args:
            productId (int): 产品ID，如果为None则忽略此参数。
            productName (str): 产品名称，如果为None则忽略此参数。
        Returns:
            float | None: 产品价格，如果查询过程中发生异常，则返回None。
        Raises:
            无。
        """
        if productId is None and productName is None:
            return None
        try:
            connection = get_db_connection()
            with connection.cursor() as cursor:
                if productId is not None:
                    cursor.execute('SELECT ProductPrice FROM Product WHERE ProductId = %s', productId)
                else:
                    cursor.execute('SELECT ProductPrice FROM Product WHERE ProductName = %s', productName)
                return cursor.fetchone()['ProductPrice']
        except Exception as e:
            print(f"Error: {e}")
            return None

    @staticmethod
    def getProductInventory(productId=None, productName=None):
        """
        根据产品ID或名称获取产品库存。
        Args:
            productId (int): 产品ID，如果为None则忽略此参数。
            productName (str): 产品名称，如果为None则忽略此参数。
        Returns:
            int | None: 产品库存，如果查询过程中发生异常，则返回None。
        Raises:
            无。
        """
        if productId is None and productName is None:
            return None
        try:
            connection = get_db_connection()
            with connection.cursor() as cursor:
                if productId is not None:
                    cursor.execute('SELECT ProductInventory FROM Product WHERE ProductId = %s', productId)
                else:
                    cursor.execute('SELECT ProductInventory FROM Product WHERE ProductName = %s', productName)
                return cursor.fetchone()['ProductInventory']
        except Exception as e:
            print(f"Error: {e}")
            return None

    @staticmethod
    def addNewProduct(productName, productPrice, productInventory):
        """
        添加新产品。
        Args:
            productName (str): 产品名称。
            productPrice (float): 产品价格。
            productInventory (int): 产品库存。
        Returns:
            bool: 是否添加成功。
        Raises:
            无。
        """
        try:
            connection = get_db_connection()
            with connection.cursor() as cursor:
                cursor.execute('INSERT INTO Product (ProductName, ProductPrice, ProductInventory) '
                               'VALUES (%s, %s, %s)', (productName, productPrice, productInventory))
                connection.commit() # 提交事务
                return True
        except Exception as e:
            print(f"Error: {e}")
            return False

    @staticmethod
    def deleteProduct(productId=None, productName=None):
        """
        根据产品ID或名称删除产品。
        Args:
            productId (int): 产品ID，如果为None则忽略此参数。
            productName (str): 产品名称，如果为None则忽略此参数。
        Returns:
            bool: 是否删除成功。
        Raises:
            无。
        """
        if productId is None and productName is None:
            return False
        try:
            connection = get_db_connection()
            with connection.cursor() as cursor:
                if productId is not None:
                    cursor.execute('DELETE FROM Product WHERE ProductId = %s', productId)
                else:
                    cursor.execute('DELETE FROM Product WHERE ProductName = %s', productName)
                connection.commit() # 提交事务
                return True
        except Exception as e:
            print(f"Error: {e}")
            return False

    @staticmethod
    def updateProductName(productId=None, productName=None, newName=None):
        """
        根据产品ID或名称修改产品名称。
        Args:
            productId (int): 产品ID，如果为None则忽略此参数。
            productName (str): 产品名称，如果为None则忽略此参数。
            newName (str): 新产品名称，如果为None则忽略此参数。
        Returns:
            bool: 是否修改成功。
        Raises:
            无。
        """
        if productId is None and productName is None:
            return False

        if newName is None:
            return False
        try:
            connection = get_db_connection()
            with connection.cursor() as cursor:
                if productId is not None:
                    cursor.execute('UPDATE Product SET ProductName = %s WHERE ProductId = %s', (newName, productId))
                else:
                    cursor.execute('UPDATE Product SET ProductName = %s WHERE ProductName = %s', (newName, productName))
                connection.commit() # 提交事务
                return True
        except Exception as e:
            print(f"Error: {e}")
            return False

    @staticmethod
    def updateProductPrice(productId=None, productName=None, newPrice=None):
        """
        根据产品ID或名称修改产品价格。
        Args:
            productId (int): 产品ID，如果为None则忽略此参数。
            productName (str): 产品名称，如果为None则忽略此参数。
            newPrice (float): 新产品价格，如果为None则忽略此参数。
        Returns:
            bool: 是否修改成功。
        Raises:
            无。
        """
        if productId is None and productName is None:
            return False

        if newPrice is None:
            return False
        try:
            connection = get_db_connection()
            with connection.cursor() as cursor:
                if productId is not None:
                    cursor.execute('UPDATE Product SET ProductPrice = %s WHERE ProductId = %s', (newPrice, productId))
                else:
                    cursor.execute('UPDATE Product SET ProductPrice = %s WHERE ProductName = %s', (newPrice, productName))
                connection.commit() # 提交事务
                return True
        except Exception as e:
            print(f"Error: {e}")
            return False


    @staticmethod
    def updateProductInventory(productId=None, productName=None, newInventory=None):
        """
        根据产品ID或名称修改产品库存。
        Args:
            productId (int): 产品ID，如果为None则忽略此参数。
            productName (str): 产品名称，如果为None则忽略此参数。
            newInventory (int): 新产品库存，如果为None则忽略此参数。
        Returns:
            bool: 是否修改成功。
        Raises:
            无。
        """
        if productId is None and productName is None:
            return False

        if newInventory is None:
            return False
        try:
            connection = get_db_connection()
            with connection.cursor() as cursor:
                if productId is not None:
                    cursor.execute('UPDATE Product SET ProductInventory = %s WHERE ProductId = %s', (newInventory, productId))
                else:
                    cursor.execute('UPDATE Product SET ProductInventory = %s WHERE ProductName = %s', (newInventory, productName))
                connection.commit() # 提交事务
                return True
        except Exception as e:
            print(f"Error: {e}")
            return False

    @staticmethod
    def getProductColumns():
        """
        获取产品表列名。
        Args:
            无。
        Returns:
            list[str]: 产品表列名列表。
        Raises:
            无。
        """
        try:
            connection = get_db_connection()
            with connection.cursor() as cursor:
                cursor.execute('SELECT * FROM Product LIMIT 1')
                columns = [column[0] for column in cursor.description] # 获取列名
                return columns
        except Exception as e:
            print(f"Error: {e}")
            return []

class ProductService():
    def __init__(self):
        pass

    def getProductsInfo(self):
        """
        获取所有产品信息。
        Args:
            无。
        Returns:
            list[dict]: 产品信息列表，每个元素为一个字典。
        Raises:
            无。
        """
        rows = ProductRepository.getAllProducts()
        return rows

    def getProductsNames(self):
        """
        获取所有产品名称。
        Args:
            无。
        Returns:
            list[str]: 产品名称列表。
        Raises:
            无。
        """
        return ProductRepository.getAllProductsName()

    def getProductsPrice(self):
        """
        获取所有产品价格。
        Args:
            无。
        Returns:
            list[float]: 产品价格列表。
        Raises:
            无。
        """
        return ProductRepository.getAllProductsPrice()

    def getProductsInventory(self):
        """
        获取所有产品库存。
        Args:
            无。
        Returns:
            list[int]: 产品库存列表。
        Raises:
            无。
        """
        return ProductRepository.getAllProductsInventory()

    def isProductExist(self, productId=None, productName=None):
        """
        判断产品是否存在。
        Args:
            productId (int): 产品ID，如果为None则忽略此参数。
            productName (str): 产品名称，如果为None则忽略此参数。
        Returns:
            bool: 是否存在。
        Raises:
            无。
        """
        if productId is None and productName is None:
            return False
        if ProductRepository.getProductId(productName=productName) is None:
            return False
        else:
            return True

    def deleteProduct(self, productId=None, productName=None):
        """
        删除产品。
        Args:
            productId (int): 产品ID，如果为None则忽略此参数。
            productName (str): 产品名称，如果为None则忽略此参数。
        Returns:
            bool: 是否删除成功。
        Raises:
            无。
        """
        if productId is None and productName is None:
            return False
        if ProductRepository.getProductId(productId=productId, productName=productName) is None:
            return False
        return ProductRepository.deleteProduct(productId=productId, productName=productName)

    def updateProductName(self, productId=None, productName=None, newName=None):
        """
        更新产品名称。
        Args:
            productId (int): 产品ID，如果为None则忽略此参数。
            productName (str): 产品名称，如果为None则忽略此参数。
            newName (str): 新名称。
        Returns:
            bool: 是否更新成功。
        Raises:
            无。
        """
        return ProductRepository.updateProductName(productId=productId, productName=productName, newName=newName)

    def updateProductPrice(self, productId=None, productName=None, newPrice=None):
        """
        更新产品价格。
        Args:
            productId (int): 产品ID，如果为None则忽略此参数。
            productName (str): 产品名称，如果为None则忽略此参数。
            newPrice (float): 新价格。
        Returns:
            bool: 是否更新成功。
        Raises:
            无。
        """
        return ProductRepository.updateProductPrice(productId=productId, productName=productName, newPrice=newPrice)

    def updateProductInventory(self, productId=None, productName=None, newInventory=None):
        """
        更新产品库存。
        Args:
            productId (int): 产品ID，如果为None则忽略此参数。
            productName (str): 产品名称，如果为None则忽略此参数。
            newInventory (int): 新库存。
        Returns:
            bool: 是否更新成功。
        Raises:
            无。
        """
        return ProductRepository.updateProductInventory(productId=productId, productName=productName, newInventory=newInventory)

    def addNewProduct(self, productName=None, productPrice=None, productInventory=None):
        """
        添加新产品。
        Args:
            productName (str): 产品名称，如果为None则忽略此参数。
            productPrice (float): 产品价格，如果为None则忽略此参数。
            productInventory (int): 产品库存，如果为None则忽略此参数。
        Returns:
            bool: 是否添加成功。
        Raises:
            无。
        """
        if productName is None or productPrice is None or productInventory is None:
            return False # 参数不能为None

        return ProductRepository.addNewProduct(productName=productName, productPrice=productPrice, productInventory=productInventory)

    def getProductInfo(self, productName=None):
        """
        获取产品信息。
        Args:
            productName (str): 产品名称，如果为None则忽略此参数。
        Returns:
            dict: 产品信息。
        Raises:
            无。
        """
        return ProductRepository.getProductInformation(productName=productName)

    def getProductName(self, productId=None, productName=None):
        """
        获取产品名称。
        Args:
            productId (int): 产品ID，如果为None则忽略此参数。
            productName (str): 产品名称，如果为None则忽略此参数。
        Returns:
            str: 产品名称。
        Raises:
            无。
        """
        return ProductRepository.getProductName(productId=productId, productName=productName)

    def getProductPrice(self, productId=None, productName=None):
        """
        获取产品价格。
        Args:
            productId (int): 产品ID，如果为None则忽略此参数。
            productName (str): 产品名称，如果为None则忽略此参数。
        Returns:
            float: 产品价格。
        Raises:
            无。
        """
        return ProductRepository.getProductPrice(productId=productId, productName=productName)

    def getProductInventory(self, productId=None, productName=None):
        """
        获取产品库存。
        Args:
            productId (int): 产品ID，如果为None则忽略此参数。
            productName (str): 产品名称，如果为None则忽略此参数。
        Returns:
            int: 产品库存。
        Raises:
            无。
        """
        return ProductRepository.getProductInventory(productId=productId, productName=productName)

    def addNewProducts(self, newProducts=None):
        """
        批量添加新产品。
        Args:
            newProducts (list): 新产品列表，每个元素是一个字典，包含键为productName、productPrice和productInventory的项。
        Returns:
            bool: 是否添加成功。
        Raises:
            无。
        """
        if newProducts is None and 0 == len(newProducts):
            return False # 新产品列表为空
        for newProduct in newProducts:
            name = newProduct['productName']
            price = newProduct['productPrice']
            inventory = newProduct['productInventory']
            if not ProductRepository.addNewProduct(productName=name, productPrice=price, productInventory=inventory):
                return False # 添加失败
        return True # 添加成功

