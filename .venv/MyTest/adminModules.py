import pymysql
import MyTest.ProductModule as ProductModule
import MyTest.VendorsModule as VendorsModule
import MyTest.SupplierProductModule as SupplierProductModule
import MyTest.InteractiveModules as InteractiveModules
from MyTest.db_connection import get_db_connection

class GeneralAdmin():
    def __init__(self, userName, userId, userType, adminAccount):
        self.userName = userName
        self.userId = userId
        self.userType = userType
        self.adminAccount = adminAccount

        self.supplierProduct = SupplierProductModule.SupplierProductService()
        self.vendorInfo = VendorsModule.VendorsService()
        self.productInfo = ProductModule.ProductService()

    def showProducts(self):
        """
        展示所有产品信息。
        Args:
            无参数。
        Returns:
            bool: 展示成功返回True，失败返回False。

        """
        supplierProduct = self.supplierProduct.getSupplierProducts()
        if supplierProduct is None:
            print('No products in the database.')
            return False
        max_width = 20
        products = [
            (row['SupplierProductId'], ProductModule.ProductRepository.getProductName(productId=row['ProductId']), VendorsModule.VendorsRepository.getVendorName(vendorId=row['VendorId']), row['SupplyPrice'], row['SupplyInventory'], row['SupplyDate'].strftime('%Y-%m-%d'), row['ExpirationDays'].strftime('%Y-%m-%d'))
            for row in supplierProduct
        ]
        columns = ('SupplierProductId', 'ProductName', 'VendorName', 'SupplyPrice', 'SupplyInventory', 'SupplyDate', 'ExpirationDays')
        print(f'{"".join([f"{item:<{max_width}} " for item in columns])}')
        width = 19
        for row in products:
            formatted_row = '  '.join([f'{item:<{width}}' for item in row])
            print(formatted_row)
        return True

    def showProductsByVendor(self, vendorId=None, vendorName=None):
        """
        根据供应商名称或ID展示产品信息。
        Args:
            vendorId (int, optional): 供应商ID。 Defaults to None.
            vendorName (str, optional): 供应商名称。 Defaults to None.
        Returns:
            bool: 展示成功返回True，失败返回False。
        """
        supplierProduct = self.supplierProduct.getSupplierProducts()
        supplyProducts = self.supplierProduct.getProductByVendorName(vendorId=vendorId, vendorName=vendorName)
        if supplyProducts is None:
            print('No products in the database.')
            return False
        if vendorId is None:
            name = vendorName
        else:
            name = VendorsModule.VendorsRepository.getVendorName(vendorId=vendorId)
        max_widths = [max(len(str(item[col])) for item in data) for col in range(len(supplierProduct[0]))]
        products = [
            (row[0], ProductModule.ProductRepository.getProductName(productId=row[1]),
             name, row[3], row[4], row[5], row[6])
            for row in supplierProduct
        ]
        columns = ('SupplierProductId', 'ProductName', 'VendorName', 'SupplyPrice', 'SupplyInventory', 'SupplyDate', 'ExpirationDays')
        print(f'{"".join([f"{item:<{width}} " for item, width in zip(columns, max_widths)])}')
        for row in products:
            formatted_row = '  '.join([f'{item:<{width}}' for item, width in zip(row, max_widths)])
            print(formatted_row)
        return True

    def showProductsOnSale(self):
        """
        展示所有在售的产品信息。
        Args:
            无参数。
        Returns:
            bool: 展示成功返回True，失败返回False。
        """
        products = self.productInfo.getProductsInfo()
        if products is None:
            print('No products in the database.')
            return False
        columns = ProductModule.ProductRepository.getProductColumns()
        max_widths = {col: max(len(str(products[0][col])), len(col)) for col in columns}
        for product in products[1:]:  # 跳过第一个产品，因为我们已经用它来获取了键
            for col in columns:
                max_widths[col] = max(max_widths[col], len(str(product[col])))

        # 创建表头
        header_format = '  '.join(f'{{:<{width}}}' for width in max_widths.values())
        print(header_format.format(*columns))

        # 创建分隔线（可选）
        separator = '-' * (sum(max_widths.values()) + len(columns) - 1)
        print(separator)

        # 打印每行数据
        row_format = '  '.join(f'{{:<{width}}}' for width in max_widths.values())
        for product in products:
            print(row_format.format(*(str(product[col]) for col in columns)))

        return True

    def showVendorsInfo(self):
        """
        展示所有供应商信息。
        Args:
            无参数。
        Returns:
            bool: 展示成功返回True，失败返回False。
        """
        vendors = self.vendorInfo.getVendorsInfo()
        if vendors is None:
            print('No products in the database.')
            return False
        columns = VendorsModule.VendorsRepository.getVendorsColumns()
        max_widths = {col: max(len(str(vendors[0][col])), len(col)) for col in columns}
        for vendor in vendors[1:]:  # 跳过第一个产品，因为我们已经用它来获取了键
            for col in columns:
                max_widths[col] = max(max_widths[col], len(str(vendor[col])))
        # 创建表头
        header_format = '  '.join(f'{{:<{width}}}' for width in max_widths.values())
        print(header_format.format(*columns))
        # 创建分隔线（可选）
        separator = '-' * (sum(max_widths.values()) + len(columns) - 1)
        print(separator)
        # 打印每行数据
        row_format = '  '.join(f'{{:<{width}}}' for width in max_widths.values())
        for vendor in vendors:
            print(row_format.format(*(str(vendor[col]) for col in columns)))
        return True

    def showProductInfo(self, vendorName=None, productName=None):
        """
        展示指定供应商的指定产品信息。
        Args:
            vendorName (str): 供应商名称。
            productName (str): 产品名称。
        Returns:
            bool: 展示成功返回True，失败返回False。
        """

        productInfo = self.supplierProduct.getProductInfo(productName=productName, vendorName=vendorName)
        if productInfo is None:
            print('No products in the database.')
            return False
        maxWidth = 10
        columns = ('SupplierProductId', 'ProductName', 'VendorName', 'SupplyPrice', 'SupplyInventory', 'SupplyDate', 'ExpirationDays')
        print(f'{"".join([f"{item:<{maxWidth}} " for item in columns])}')
        print(f'{"".join([f"{item:<{maxWidth}} " for item in productInfo])}')
        return True

    def showVendorInfo(self, vendorName=None):
        """
        展示指定供应商信息。
        Args:
            vendorName (str): 供应商名称。
        Returns:
            bool: 展示成功返回True，失败返回False。
        """
        vendorInfo = self.vendorInfo.getVendorInfo(vendorName=vendorName)
        if vendorInfo is None:
            print('No products in the database.')
            return False
        maxWidth = 10
        columns = ('VendorId', 'VendorName', 'Address')
        print(f'{"".join([f"{item:<{maxWidth}} " for item in columns])}')
        print(f'{"".join([f"{item:<{maxWidth}} " for item in vendorInfo])}')
        return True


    def changeProductInventory(self, productName=None, number=None):
        """
        改变货架上的指定产品库存。
        Args:
            productName (str): 产品名称。
            number (int): 增加或减少的库存数量。
        Returns:
            bool: 操作成功返回True，失败返回False。
        """
        if number is None or number == 0:
            print('No change')
            return False
        if not self.productInfo.isProductExist(productName=productName):
            return False # 不存在该产品
        newInventory = self.productInfo.getProductInventory(productName=productName) + number
        if newInventory is None or newInventory < 0:
            print('库存不足')
            return False # 库存不足
        if self.productInfo.updateProductInventory(productName=productName, newInventory=newInventory):
            print('更新成功')
            return True # 更新成功
        else:
            print('更新失败')
            return False # 更新失败

    def changeProductPrice(self, productName=None, newPrice=None):
        """
        改变指定产品价格。
        Args:
            productName (str): 产品名称。
            newPrice (float): 新的价格。
        Returns:
            bool: 操作成功返回True，失败返回False。
        """
        if newPrice is None or newPrice == 0:
            print('ERROR')
            return False
        if self.productInfo.updateProductPrice(productName=productName, newPrice=newPrice):
            print('更新成功')
            return True # 更新成功
        else:
            print('更新失败')
            return False

    def changeProductName(self, productName=None, newName=None):
        """
        改变指定产品名称。
        Args:
            productName (str): 产品名称。
            newName (str): 新的产品名称。
        Returns:
            bool: 操作成功返回True，失败返回False。
        """
        if newName is None or newName == productName:
            print('ERROR')
            return False
        if self.productInfo.updateProductName(productName=productName, newName=newName):
            print('更新成功')
            return True # 更新成功
        else:
            print('更新失败')
            return False

    def addNewProduct(self, productName=None, productPrice=None, productInventory=None):
        """
        添加新的产品。
        Args:
            productName (str): 产品名称。
            productPrice (float): 产品价格。
            productInventory (int): 产品库存。
        Returns:
            bool: 操作成功返回True，失败返回False。
        """
        if self.productInfo.addNewProduct(productName=productName, productPrice=productPrice, productInventory=productInventory):
            print('添加成功')
            return True # 添加成功
        else:
            print('添加失败')
            return False

    def addNewSupplierProduct(self, productName=None, vendorName=None, supplyPrice=None, supplyInventory=None, supplyDate=None, expirationDays=None):
        """
        添加新的供应商产品。
        Args:
            productName (str): 产品名称。
            vendorName (str): 供应商名称。
            supplyPrice (float): 供应价格。
            supplyInventory (int): 供应库存。
            supplyDate (date): 供应日期。
            expirationDays (int): 有效期（天数）。
        Returns:
            bool: 操作成功返回True，失败返回False。
        """
        if productName is None or vendorName is None:
            print('ERROR')
            return False
        if supplyPrice is None or supplyInventory is None:
            print('ERROR')
            return False
        if supplyDate is None or expirationDays is None:
            print('ERROR')
            return False
        products = {
            'productName': productName,
            'vendorName': vendorName,
            'supplyPrice': supplyPrice,
            'supplyInventory': supplyInventory,
            'supplyDate': supplyDate,
            'expirationDays': expirationDays
        }
        if self.supplierProduct.addNewProduct(products):
            print('添加成功')
            return True
        else:
            print('添加失败')
            return False

class SuperAdmin(GeneralAdmin):
    def __init__(self, userName, userId, userType, adminAccount):
        super().__init__(userName, userId, userType, adminAccount)

    def changeVendorName(self, vendorId=None, vendorName=None, newName=None):
        """
        改变指定供应商名称。
        Args:
            vendorId (int): 供应商ID。
            vendorName (str): 供应商名称。
            newName (str): 新的供应商名称。
        Returns:
            bool: 操作成功返回True，失败返回False。
        """
        if newName is None or newName == vendorName:
            print('ERROR')
            return False
        if self.vendorInfo.updateVendorName(vendorId=vendorId, vendorName=vendorName, newName=newName):
            print('更新成功')
            return True # 更新成功
        else:
            print('更新失败')
            return False

    def changeVendorAddress(self, vendorId=None, vendorName=None, newAddress=None):
        """
        改变指定供应商地址。
        Args:
            vendorId (int): 供应商ID。
            vendorName (str): 供应商名称。
            newAddress (str): 新的供应商地址。
        Returns:
            bool: 操作成功返回True，失败返回False。
        """
        if newAddress is None or newAddress == vendorName:
            print('ERROR')
            return False
        if self.vendorInfo.updateVendorAddress(vendorId=vendorId, vendorName=vendorName, newAddress=newAddress):
            print('更新成功')
            return True # 更新成功
        else:
            print('更新失败')
            return False

    def addNewVendor(self, vendorName=None, vendorAddress=None):
        """
        添加新的供应商。
        Args:
            vendorName (str): 供应商名称。
            vendorAddress (str): 供应商地址。
        Returns:
            bool: 操作成功返回True，失败返回False。
        """
        if VendorsModule.VendorsRepository.addNewVendor(vendorName=vendorName, vendorAddress=vendorAddress):
            print('添加成功')
            return True # 添加成功
        else:
            print('添加失败')
            return False

    def deleteSupplierProduct(self, vendorName=None, productName=None):
        """
        删除指定供应商产品。
        Args:
            vendorName (str): 供应商名称。
            productName (str): 产品名称。
        Returns:
            bool: 操作成功返回True，失败返回False。
        """
        if self.supplierProduct.deleteProduct(productName=productName, vendorName=vendorName):
            print('删除成功')
            return True
        else:
            print('删除失败')
            return False

    def deleteProduct(self, productId=None, productName=None):
        """
        删除指定产品。
        Args:
            productId (int): 产品ID。
            productName (str): 产品名称。
        Returns:
            bool: 操作成功返回True，失败返回False。
        """
        if self.productInfo.deleteProduct(productId=productId, productName=productName):
            print('删除成功')
            return True
        else:
            print('删除失败')
            return False

    def deleteVendor(self, vendorId=None, vendorName=None):
        """
        删除指定供应商。
        Args:
            vendorId (int): 供应商ID。
            vendorName (str): 供应商名称。
        Returns:
            bool: 操作成功返回True，失败返回False。
        """
        if self.vendorInfo.deleteVendor(vendorId=vendorId, vendorName=vendorName):
            print('删除成功')
            return True
        else:
            print('删除失败')
            return False

    def changeProductSupplyPrice(self, productName=None, vendorName=None, supplyPrice=None):
        """
        改变指定供应商产品价格。
        Args:
            productName (str): 产品名称。
            vendorName (str): 供应商名称。
            supplyPrice (float): 新的供应价格。
        Returns:
            bool: 操作成功返回True，失败返回False。
        """
        if supplyPrice is None or supplyPrice == 0:
            print('ERROR')
            return False
        if self.supplierProduct.updateProductPrice(productName=productName, vendorName=vendorName, newPrice=supplyPrice):
            print('更新成功')
            return True # 更新成功
        else:
            print('更新失败')
            return False

    def changeSupplyDate(self, productName=None, vendorName=None, supplyDate=None):
        """
        改变指定供应商产品供应日期。
        Args:
            productName (str): 产品名称。
            vendorName (str): 供应商名称。
            supplyDate (datetime.date): 新的供应日期。
        Returns:
            bool: 操作成功返回True，失败返回False。
        """
        if supplyDate is None:
            print('ERROR')
            return False
        if self.supplierProduct.updateProductSupplyDate(productName=productName, vendorName=vendorName, newSupplyDate=supplyDate):
            print('更新成功')
            return True # 更新成功
        else:
            print('更新失败')
            return False

