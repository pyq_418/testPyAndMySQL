import pymysql
from  MyTest.db_connection import get_db_connection
import MyTest.adminModules as adminModules

# 验证身份，用于管理员登录
class AuthenticationService():
    # 构造函数
    def __init__(self, adminAccount, adminPassword):
        self._adminAccount = adminAccount # 管理员账号
        self._adminPassword = adminPassword # 管理员密码

    @staticmethod
    def getAdminMessage(AdminAccount):
        """
        获取管理员账号。
        Args:
            AdminAccount: 管理员账号。
        Returns:
            dict or None: 如果找到管理员账号，则返回包含管理员账号信息的字典；否则返回 None
        """
        conn = get_db_connection(); # 获取数据库连接
        with conn.cursor() as cursor:
            sql_admin = "SELECT * FROM Admin WHERE AdminAccount = %s" # 查询管理员账号
            cursor.execute(sql_admin, AdminAccount)
            result = cursor.fetchone()
            #conn.close()
        return result # 获取查询结果

    @staticmethod
    def getUserMessage(UserId):
        """
        获取用户信息。
        Args:
            UserId: 用户编号。
        Returns:
            dict or None: 如果找到用户，则返回包含用户信息的字典；否则返回 None
        """
        conn = get_db_connection(); # 获取数据库连接
        with conn.cursor() as cursor:
            sql_user = "SELECT * FROM User WHERE UserId = %s" # User表内管理员的密码
            cursor.execute(sql_user, UserId)
            result = cursor.fetchone()
            #conn.close()
            return result # 获取查询结果

    # 验证管理员身份
    def verifyIdentity(self):
        """
        验证管理员身份是否合法。
        Args:
            无参数。
        Returns:
            bool: 如果管理员账号和密码均正确，则返回True；否则返回False。
        """

        adminRecord = AuthenticationService.getAdminMessage(self._adminAccount) # 获取查询结果

        if adminRecord is not None:
            userRecord = AuthenticationService.getUserMessage(adminRecord['UserId']) # 获取查询结果

            if userRecord is not None and self._adminPassword == userRecord['UserPwd']:
                return True
            else:
                return False
        else:
            return False



    def getAdminAccount(self):
        """
        获取管理员账号。
        Args:
            无参数。
        Returns:
            str: 返回管理员账号。
        """
        return self._adminAccount


class UserSession():
    def __init__(self, AdminAccount):
        self._adminAccount = AdminAccount  # 管理员账号
        adminRecord = AuthenticationService.getAdminMessage(self._adminAccount)
        if adminRecord is not None:
            self._userId = adminRecord['UserId']  # 用户编号
            userRecord = AuthenticationService.getUserMessage(adminRecord['UserId'])
            self._userName = userRecord['UserName']  # 用户名
            self._userType = userRecord['UserType']  # 用户类型

            if 0 == self._userType:
                self._admin = adminModules.SuperAdmin(userId=self._userId, userName=self._userName, userType=self._userType, adminAccount=AdminAccount)
            elif 1 == self._userType:
                self._admin = adminModules.GeneralAdmin(userId=self._userId, userName=self._userName, userType=self._userType, adminAccount=AdminAccount)

        else:
            self._userId = None
            self._userName = None
            self._userType = 1

    def getUserName(self):
        """
        获取用户名。
        Args:
            无参数。
        Returns:
            str: 返回用户名。
        """
        return self._userName

    def getUserType(self):
        """
        获取用户类型。
        Args:
            无参数。
        Returns:
            int: 返回用户类型, 0为超级管理员，1为普通管理员
        """
        return self._userType

    def getAdminAccount(self):
        """
        获取管理员账号。
        Args:
            无参数。
        Returns:
            str: 返回管理员账号。
        """
        return self._adminAccount

    def setUserName(self, newName):
        """
        设置管理员账号。
        Args:
            newName: 新的管理员名字。
        Returns:
            bool: 如果设置成功，则返回True；否则返回False。
        """
        self._userName = newName
        conn = get_db_connection(); # 获取数据库连接
        sql_update = "UPDATE User SET UserName = %s WHERE UserId = %s" # 更新用户名
        try:
            with conn.cursor() as cursor:
                cursor.execute(sql_update, (newName, self._userId))
                conn.commit() # 提交事务
                conn.close()
                return True
        except Exception as e:
            print(f"出现异常：{e}")
            conn.rollback() # 回滚事务
            #conn.close()
            return False

    def setUserPassword(self, newPassword):
        """
        设置用户密码。
        Args:
            newPassword: <PASSWORD>。
        Returns:
            bool: 如果设置成功，则返回True；否则返回False。
        """
        conn = get_db_connection(); # 获取数据库连接
        sql_update = "UPDATE User SET UserPwd = %s WHERE UserId = %s" # 更新用户密码
        try:
            with conn.cursor() as cursor:
                cursor.execute(sql_update, (newPassword, self._userId))
                conn.commit() # 提交事务
                conn.close()
                return True
        except Exception as e:
            print(f"出现异常：{e}")
            conn.rollback() # 回滚事务
            #conn.close()
            return False

    def getAdminMessage(self):
        """
        获取管理员信息。
        Args:
            无参数。
        Returns:
            dict: 返回管理员信息。
        """
        return self._admin
