import pymysql
from MyTest.db_connection import get_db_connection

class VendorsRepository:

    @staticmethod
    def getAllVendors():
        """
        从数据库中获取所有供应商信息。

        Args:
            无。

        Returns:
            List[tuple]: 包含所有供应商信息的元组列表，每个元组代表一个供应商。
            如果查询过程中发生异常，则返回None。

        Raises:
            无。
        """
        try:
            connection = get_db_connection()
            with connection.cursor() as cursor:
                sql = "SELECT * FROM vendors"
                cursor.execute(sql)
                return cursor.fetchall()
        except Exception as e:
            print(f"Error: {e}")
            return None

    @staticmethod
    def getAllVendorsName():
        """
        从数据库中获取所有供应商名称。
        Args:
            无。
        Returns:
            List[str]: 包含所有供应商名称的列表，每个元素代表一个供应商名称。
            如果查询过程中发生异常，则返回None。
        Raises:
            无。
        """
        try:
            connection = get_db_connection()
            with connection.cursor() as cursor:
                sql = "SELECT VendorName FROM vendors"
                cursor.execute(sql)
                return [row['VendorName'] for row in cursor.fetchall()]
        except Exception as e:
            print(f"Error: {e}")
            return None

    @staticmethod
    def getAllVendorsAddress():
        """
        从数据库中获取所有供应商地址。
        Args:
            无。
        Returns:
            List[str]: 包含所有供应商地址的列表，每个元素代表一个供应商地址。
            如果查询过程中发生异常，则返回None。
        Raises:
            无。
        """
        try:
            connection = get_db_connection()
            with connection.cursor() as cursor:
                sql = "SELECT VendorAddress FROM vendors"
                cursor.execute(sql)
                return [row['VendorAddress'] for row in cursor.fetchall()]
        except Exception as e:
            print(f"Error: {e}")
            return None

    @staticmethod
    def getVendorInformation(vendorId=None, vendorName=None):
        """
        根据供应商ID或名称获取供应商信息。
        Args:
            vendorId (int, optional): 供应商ID，如果为None则忽略该参数。 Defaults to None.
            vendorName (str, optional): 供应商名称，如果为None则忽略该参数。 Defaults to None.
        Returns:
            tuple | None: 如果查询成功，返回包含供应商信息的元组；否则返回None。
        Raises:
            无。
        """
        if vendorId is None and vendorName is None:
            return None
        try:
            connection = get_db_connection()
            with connection.cursor() as cursor:
               if vendorId is not None:
                   cursor.execute('SELECT * FROM vendors WHERE VendorId = %s', (vendorId,))
               else:
                   cursor.execute('SELECT * FROM vendors WHERE VendorName = %s', (vendorName,))
               return cursor.fetchone()
        except Exception as e:
            print(f"Error: {e}")
            return None

    @staticmethod
    def getVendorId(vendorId=None, vendorName=None):
        """
        根据供应商ID或名称获取供应商ID。
        Args:
            vendorId (int, optional): 供应商ID，如果为None则忽略该参数。 Defaults to None.
            vendorName (str, optional): 供应商名称，如果为None则忽略该参数。 Defaults to None.
        Returns:
            int | None: 如果查询成功，返回供应商ID；否则返回None。
        Raises:
            无。
        """
        if vendorId is None and vendorName is None:
            return None
        try:
            connection = get_db_connection()
            with connection.cursor() as cursor:
               if vendorId is not None:
                   cursor.execute('SELECT VendorId FROM vendors WHERE VendorId = %s', (vendorId,))
               else:
                   cursor.execute('SELECT VendorId FROM vendors WHERE VendorName = %s', (vendorName,))
               return cursor.fetchone()['VendorId']
        except Exception as e:
            print(f"Error: {e}")
            return None

    @staticmethod
    def getVendorName(vendorId=None, vendorName=None):
        """
        根据供应商ID或名称获取供应商名称。
        Args:
            vendorId (int, optional): 供应商ID，如果为None则忽略该参数。 Defaults to None.
            vendorName (str, optional): 供应商名称，如果为None则忽略该参数。 Defaults to None.
        Returns:
            str | None: 如果查询成功，返回供应商名称；否则返回None。
        Raises:
            无。
        """
        if vendorId is None and vendorName is None:
            return None
        try:
            connection = get_db_connection()
            with connection.cursor() as cursor:
               if vendorId is not None:
                   cursor.execute('SELECT VendorName FROM vendors WHERE VendorId = %s', (vendorId,))
               else:
                   cursor.execute('SELECT VendorName FROM vendors WHERE VendorName = %s', (vendorName,))
               return cursor.fetchone()['VendorName']
        except Exception as e:
            print(f"Error: {e}")
            return None

    @staticmethod
    def getVendorAddress(vendorId=None, vendorName=None):
        """
        根据供应商ID或名称获取供应商地址。
        Args:
            vendorId (int, optional): 供应商ID，如果为None则忽略该参数。 Defaults to None.
            vendorName (str, optional): 供应商名称，如果为None则忽略该参数。 Defaults to None.
        Returns:
            str | None: 如果查询成功，返回供应商地址；否则返回None。
        Raises:
            无。
        """
        if vendorId is None and vendorName is None:
            return None
        try:
            connection = get_db_connection()
            with connection.cursor() as cursor:
               if vendorId is not None:
                   cursor.execute('SELECT VendorAddress FROM vendors WHERE VendorId = %s', (vendorId,))
               else:
                   cursor.execute('SELECT VendorAddress FROM vendors WHERE VendorName = %s', (vendorName,))
               return cursor.fetchone()['VendorAddress']
        except Exception as e:
            print(f"Error: {e}")
            return None

    @staticmethod
    def addNewVendor(vendorName=None, vendorAddress=None):
        """
        添加一个新的供应商。
        Args:
            vendorName (str, optional): 供应商名称，如果为None则忽略该参数。 Defaults to None.
            vendorAddress (str, optional): 供应商地址，如果为None则忽略该参数。 Defaults to None.
        Returns:
            bool: 是否添加成功。
        Raises:
            无。
        """
        if vendorName is None or vendorAddress is None:
            return False # 数据不能为空
        try:
            connection = get_db_connection()
            with connection.cursor() as cursor:
                cursor.execute('INSERT INTO vendors(VendorName, VendorAddress) '
                               'VALUES(%s, %s)', (vendorName, vendorAddress))
                connection.commit() # 提交事务
                return True
        except Exception as e:
            print(f"Error: {e}")
            return False

    @staticmethod
    def deleteVendor(vendorId=None, vendorName=None):
        """
        根据供应商ID或名称删除供应商。
        Args:
            vendorId (int, optional): 供应商ID，如果为None则忽略该参数。 Defaults to None.
            vendorName (str, optional): 供应商名称，如果为None则忽略该参数。 Defaults to None.
        Returns:
            bool: 是否删除成功。
        Raises:
            无。
        """
        if vendorId is None and vendorName is None:
            return False # 数据不能为空
        try:
            connection = get_db_connection()
            with connection.cursor() as cursor:
               if vendorId is not None:
                   cursor.execute('DELETE FROM vendors WHERE VendorId = %s', (vendorId,))
               else:
                   cursor.execute('DELETE FROM vendors WHERE VendorName = %s', (vendorName,))
               connection.commit() # 提交事务
               return True
        except Exception as e:
            print(f"Error: {e}")
            return False

    @staticmethod
    def updateVendorName(vendorId=None, vendorName=None, newName=None):
        """
        根据供应商ID或名称更新供应商名称。
        Args:
            vendorId (int, optional): 供应商ID，如果为None则忽略该参数。 Defaults to None.
            vendorName (str, optional): 供应商名称，如果为None则忽略该参数。 Defaults to None.
            newName (str, optional): 新名称，如果为None则忽略该参数。 Defaults to None.
        Returns:
            bool: 是否更新成功。
        Raises:
            无。
        """
        if vendorId is None and vendorName is None:
            return False # 数据不能为空
        if VendorsRepository.getVendorId(vendorId=vendorId, vendorName=vendorName) is None:
            return False # 供应商不存在
        if vendorName is None:
            return False # 供应商名称不能为空
        try:
            connection = get_db_connection()
            with connection.cursor() as cursor:
                if vendorId is not None:
                    cursor.execute('UPDATE vendors SET VendorName = %s WHERE VendorId = %s', (newName, vendorId))
                else:
                    cursor.execute('UPDATE vendors SET VendorName = %s WHERE VendorName = %s', (newName, vendorName))
                connection.commit() # 提交事务
                return True
        except Exception as e:
            print(f"Error: {e}")
            return False

    @staticmethod
    def updateVendorAddress(vendorId=None, vendorName=None, newAddress=None):
        """
        根据供应商ID或名称更新供应商地址。
        Args:
            vendorId (int, optional): 供应商ID，如果为None则忽略该参数。 Defaults to None.
            vendorName (str, optional): 供应商名称，如果为None则忽略该参数。 Defaults to None.
            newAddress (str, optional): 新地址，如果为None则忽略该参数。 Defaults to None.
        Returns:
            bool: 是否更新成功。
        Raises:
            无。
        """
        if vendorId is None and vendorName is None:
            return False # 数据不能为空
        if VendorsRepository.getVendorId(vendorId=vendorId, vendorName=vendorName) is None:
            return False # 供应商不存在
        if newAddress is None:
            return False # 供应商地址不能为空
        try:
            connection = get_db_connection()
            with connection.cursor() as cursor:
                if vendorId is not None:
                    cursor.execute('UPDATE vendors SET VendorAddress = %s WHERE VendorId = %s', (newAddress, vendorId))
                else:
                    cursor.execute('UPDATE vendors SET VendorAddress = %s WHERE VendorName = %s', (newAddress, vendorName))
                connection.commit() # 提交事务
                return True
        except Exception as e:
            print(f"Error: {e}")
            return False

    @staticmethod
    def getVendorsColumns():
        """
        获取供应商列。
        Args:
            无。
        Returns:
            list[str]: 供应商列。
        Raises:
            无。
        """
        try:
            connection = get_db_connection()
            with connection.cursor() as cursor:
                cursor.execute('SELECT * FROM vendors LIMIT 1')
                columns = [column[0] for column in cursor.description] # 获取列名
                return columns
        except Exception as e:
            print(f"Error: {e}")
            return []

class VendorsService():
    def __init__(self):
        pass

    def getVendorsInfo(self):
        """
        获取所有供应商信息。
        Args:
            无。
        Returns:
            list[dict]: 供应商信息列表。
        Raises:
            无。
        """
        rows = VendorsRepository.getAllVendors()
        return rows

    def getVendorsName(self):
        """
        获取所有供应商名称。
        Args:
            无。
        Returns:
            list[str]: 供应商名称列表。
        Raises:
            无。
        """
        return VendorsRepository.getAllVendorsName()

    def getVendorsAddress(self):
        """
        获取所有供应商地址。
        Args:
            无。
        Returns:
            list[str]: 供应商地址列表。
        Raises:
            无。
        """
        return VendorsRepository.getAllVendorsAddress()


    def isVendorExist(self, vendorId=None, vendorName=None):
        """
        判断供应商是否存在。
        Args:
            vendorId (int, optional): 供应商ID，如果为None则忽略该参数。 Defaults to None.
            vendorName (str, optional): 供应商名称，如果为None则忽略该参数。 Defaults to None.
        Returns:
            bool: 是否存在。
        Raises:
            无。
        """
        if vendorId is None and vendorName is None:
            return False
        if VendorsRepository.getVendorId(vendorName=vendorName) is None:
            return False
        else:
            return True

    def deleteVendor(self, vendorId=None, vendorName=None):
        """
        删除供应商。
        Args:
            vendorId (int, optional): 供应商ID，如果为None则忽略该参数。 Defaults to None.
            vendorName (str, optional): 供应商名称，如果为None则忽略该参数。 Defaults to None.
        Returns:
            bool: 是否删除成功。
        Raises:
            无。
        """
        if vendorId is None and vendorName is None:
            return False # 数据不能为空

        return VendorsRepository.deleteVendor(vendorId=vendorId, vendorName=vendorName)

    def updateVendorName(self, vendorId=None, vendorName=None, newName=None):
        """
        更新供应商名称。
        Args:
            vendorId (int, optional): 供应商ID，如果为None则忽略该参数。 Defaults to None.
            vendorName (str, optional): 供应商名称，如果为None则忽略该参数。 Defaults to None.
            newName (str, optional): 新名称，如果为None则忽略该参数。 Defaults to None.
        Returns:
            bool: 是否更新成功。
        Raises:
            无。
        """
        return VendorsRepository.updateVendorName(vendorId, vendorName, newName)

    def updateVendorAddress(self, vendorId=None, vendorName=None, newAddress=None):
        """
        更新供应商地址。
        Args:
            vendorId (int, optional): 供应商ID，如果为None则忽略该参数。 Defaults to None.
            vendorName (str, optional): 供应商名称，如果为None则忽略该参数。 Defaults to None.
            newAddress (str, optional): 新地址，如果为None则忽略该参数。 Defaults to None.
        Returns:
            bool: 是否更新成功。
        Raises:
            无。
        """
        return VendorsRepository.updateVendorAddress(vendorId=vendorId, vendorName=vendorName, newAddress=newAddress)

    def getVendorInfo(self, vendorName=None):
        """
        获取供应商信息。
        Args:
            vendorName (str, optional): 供应商名称，如果为None则忽略该参数。 Defaults to None.
        Returns:
            dict | None: 供应商信息。
        Raises:
            无。
        """
        return VendorsRepository.getVendorInformation(vendorName=vendorName)

    def getVendorName(self, vendorId=None, vendorName=None):
        """
        获取供应商名称。
        Args:
            vendorId (int, optional): 供应商ID，如果为None则忽略该参数。 Defaults to None.
            vendorName (str, optional): 供应商名称，如果为None则忽略该参数。 Defaults to None.
        Returns:
            str | None: 供应商名称。
        Raises:
            无。
        """
        return VendorsRepository.getVendorName(vendorId=vendorId, vendorName=vendorName)

    def getVendorAddress(self, vendorId=None, vendorName=None):
        """
        获取供应商地址。
        Args:
            vendorId (int, optional): 供应商ID，如果为None则忽略该参数。 Defaults to None.
            vendorName (str, optional): 供应商名称，如果为None则忽略该参数。 Defaults to None.
        Returns:
            str | None: 供应商地址。
        Raises:
            无。
        """
        return VendorsRepository.getVendorAddress(vendorId=vendorId, vendorName=vendorName)

    def addNewVendors(self, newVendors=None):
        """
        添加新供应商。
        Args:
            newVendors (list, optional): 新供应商列表，如果为None则忽略该参数。 Defaults to None.
        Returns:
            bool: 是否添加成功。
        Raises:
            无。
        """
        if newVendors is None and 0 == len(newVendors):
            return False # 数据不能为空
        for newVendor in newVendors:
            name = newVendor['vendorName']
            address = newVendor['vendorAddress']
            if not VendorsRepository.addNewVendor(vendorName=name, vendorAddress=address):
                return False # 添加失败
        return True # 添加成功

