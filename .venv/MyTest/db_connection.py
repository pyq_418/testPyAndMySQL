import pymysql
from dbutils.pooled_db import PooledDB

DB_PARAMS = {
    'host': 'localhost',
    'port': 3306,
    'user': 'root',
    'passwd': '123456',
    'database': 'merchandise_management',
    'charset': 'utf8mb4',
    'cursorclass': pymysql.cursors.DictCursor,  # 使用字典游标
}

# 连接池
POOL = PooledDB(
    creator=pymysql,
    maxusage=None,
    mincached=1,
    maxconnections=10,
    blocking=False,
    **DB_PARAMS #基本信息
)

#用于获取数据库连接
def get_db_connection():
    try:
        return POOL.connection()
    except Exception as e:
        print(f"未知错误：{e}")
        return None
    